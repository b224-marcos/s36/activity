// Controllers contain the functions and business logic of our Express JS app.
// Meaning all the operations it can perform will be written in this file.

const Task = require("../models/Task.js");

// Controller Functions;

// Function for getting all the tasks from our DB
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

// Function for creating tasks
module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error) 
			return false
		} else if (savedTask !== null && savedTask.name == reqBody.name) {
			return "Duplicate Task found"
		} else {
			console.log(savedTask);
			return savedTask
		}
	})
};

// Function for updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		}
		result.name = newContent.name

		return result.save().then((updatedTask, error) => {
			if(error) {
				console.log(error)
				return false
			} else {
				return updatedTask
			}
		})
	})
};


// Function for deleting a task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
		if(error) {
			return false// "Cannot delete task" or "An error has occurred"
		} else {
			return deletedTask // "task deleted successfully"
		}
	})
};

//////////////////////////////////////////////////////
// ACTIVITY
/////////////////////////////////////////////////////

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}

/*// Function for updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		}
		result.name = newContent.name

		return result.save().then((updatedTask, error) => {
			if(error) {
				console.log(error)
				return false
			} else {
				return updatedTask
			}
		})
	})
};*/

module.exports.updateStatus = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return false
		}
		result.status = newContent.status
		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return false
			}
			else{
				return updatedTask
			}
		})
	})
}

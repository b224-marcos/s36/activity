// Import "express" and any controllers using the require directive
// Routes contains all endpoints for our application
// We separate the routes such that the "app.js" only contains information on our server.
// We need to use express.Router() method/function to achieve this
const express = require("express");

// The task controller allows us to use the functions defined in the "Task controller.js" file.
const TaskController = require("../controllers/TaskController.js");

// Creates a Router instance that functions as a middleware and routing system.
// Allows access to HTTP method middlewares that make it easier to creates routes for our application
const router = express.Router();


// ROUTES 
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed.
// When a route is accessed they invoke the controller functions.

// Route  to GET task - runs the getAllTasks function from the controller.
router.get("/", (req, res) => {
	TaskController.getAllTasks().then((resultFromController) => res.send(resultFromController))
});

// Route for POST tasks
router.post("/create", (req, res) => {
	TaskController.createTask(req.body).then((resultFromController) =>res.send(resultFromController))
});

//Route for UPDATE/PUT task
router.put("/:id/update", (req, res) => {
	console.log(req.params.id)
	TaskController.updateTask(req.params.id, req.body).then((
		resultFromController) => res.send(resultFromController))
});

// route for delete task
router.delete("/:id/delete", (req, res) => {
	TaskController.deleteTask(req.params.id).then((resultFromController) => res.send(resultFromController))
});

/////////////////////////////////////////////////
//ACTIVITY
/////////////////////////////////////////////////

router.get("/:id", (req, res) => {
	TaskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/complete/:id", (req, res) => {
	TaskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})







// Use "module.exports" to export the router object to be used in the app.js
module.exports = router;
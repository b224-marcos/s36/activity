// Set imports

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

//This allows us to use all the routes defined in the "taskRoutes.js"
const taskRoutes = require("./routes/taskRoutes.js");

// Express setup
const app = express();
const port = 3001;

// Initialize dotenv
dotenv.config();

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}))

// Mongoose setup
mongoose.connect(`mongodb+srv://napmarcos:${process.env.MONGODB_PW}@marcos.hsczop7.mongodb.net/s36?retryWrites=true&w=majority`, 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection
db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB!'));




// Main URI
//http://localhost:3001/tasks/<endpoint>
app.use("/tasks", taskRoutes);



app.listen(port, () => console.log(`Server is running at port: ${port} `));